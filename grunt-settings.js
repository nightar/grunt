module.exports = {
    sassMainFileName: 'style',
    cssDir:           'css/',
    cssMainFileDir:   './',
    cssMainFileName:  'style',
    jsDir:            'js/',
    imgDir:           'images/',
    sassDir:          'source/scss/',
    srcJsDir:         'source/js/',
    imgSourceDir:     'source/images/',

    // sftp server
    sftpServer:       'example.com',
    sftpPort:         '2121',
    sftpLogin:        'login',
    sftpPas:          'password',
    sftpDestination:  '/pathTo/css',

    browser : {
      // server: {
      //   baseDir: "./",
      //   index: "index.php",
      //   directory: true
      // },
        proxy: 'http://grunt',
        open: 'external',
        host: 'grunt',
        watchTask: true
    }
  };