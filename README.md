## Install:

----
```bush
composer create-project nightar/grunt --no-install
mv grunt/* ./
rmdir grunt
npm i
grunt
```
----